package sg.ncs.product.5gt.web.web.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TestVO {

    @ApiModelProperty("name")
    private String name;
}
